# Penjelasan soal-shift-sisop-modul-4-e04-2022

### SOAL 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
	
a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

d. Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

Referensi : https://www.base64encode.org/ https://rot13.com/


### JAWABAN
Pada soal ini, saya membuat fungsi cipher yang menerima parameter string. Fungsi ini akan secara otomatis mengenkripsi huruf besar dan huruf kecil sesuai dengan fungsi cipher yang diberitahu.

```c
char *cipher(char *string)
{
    char temp[1000];
    strcpy(temp, string);
    for (int i = 0; i < strlen(string); i++)
    {
        if (isupper(string[i]))
        {
            temp[i] = (char)(90 - (string[i] - 65));
        }
        else if (islower(string[i]))
        {
            if (string[i] <= 109)
                temp[i] = (char)(string[i] + 13);
            else
                temp[i] = (char)(string[i] - 13);
        }
    }
    char *result = temp;
    return result;
}
```

Saya kemudian membuat program fuse seperti pada template modul 4. Untuk enkripsinya saya lakukan pada xmp readdir dan disesuaikan dengan jenis file/direktori. Berikut merupakan fungsi deskripsi agar path yang masuk ke fungsi-fungsi xmp berupa file aslinya (true path).

```c
char *handlePath(const char *path)
{
    char fpath[2000], *rfpath, *p_path, temp_p_path[1000] = "", *p_temp_p_path;
    if (!strcmp(path, "/"))
    {
        sprintf(fpath, "%s", dirpath);
    }
    else
    {
        if (strstr(path, "Animeku_"))
        {
            p_path = strrchr(path, '/');
            strncpy(temp_p_path, path, strlen(path) - strlen(p_path));
            p_temp_p_path = strrchr(temp_p_path, '/');
            if (strstr(p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
            }
            else if (strstr(p_temp_p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "", srcFileName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                char *p_ext;
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                p_ext = strrchr(p_srcFileName, '.');
                // if file has extension (.txt, .zip, ...)
                if (p_ext)
                {
                    strncpy(srcFileName, p_srcFileName, strlen(p_srcFileName) - strlen(p_ext));
                    sprintf(fpath, "%s%s%s%s", dirpath, srcDirName, cipher(srcFileName), p_ext);
                }
                else
                {
                    char testDirPath[] = "";
                    sprintf(testDirPath, "%s%s", dirpath, path);
                    DIR *dp = opendir(testDirPath);
                    if (dp)
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
                    else
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, cipher(p_srcFileName));
                }
            }
            else
            {
                sprintf(fpath, "%s%s", dirpath, path);
            }
        }
        else
        {
            sprintf(fpath, "%s%s", dirpath, path);
        }
    }
    rfpath = fpath;
    return rfpath;
}

```

Berikut terlampir foto fuse

![img](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-05-15_22-17-03.png)

Foto Wibu.log

![img](https://gitlab.com/godlixe/assets-modul-01/-/raw/main/Screenshot_from_2022-05-15_22-17-54.png)

