#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#include <ctype.h>

char *dirpath = "/home/alex/Documents";

// encode and decode cipher and rot13 depending on upper and lowercase letter
char *cipher(char *string)
{
    char temp[1000];
    strcpy(temp, string);
    for (int i = 0; i < strlen(string); i++)
    {
        if (isupper(string[i]))
        {
            temp[i] = (char)(90 - (string[i] - 65));
        }
        else if (islower(string[i]))
        {
            if (string[i] <= 109)
                temp[i] = (char)(string[i] + 13);
            else
                temp[i] = (char)(string[i] - 13);
        }
    }
    char *result = temp;
    return result;
}

void writeLog(char *message, char *old, char *new)
{
    FILE *fptr;
    fptr = fopen("Wibu.log", "a+");

    if (fptr == NULL)
    {
        printf("Error");
        exit(1);
    }

    fprintf(fptr, "RENAME\t%s\t%s\t-->\t%s\n", message, old, new);
    fclose(fptr);
}

char *handlePath(const char *path)
{
    char fpath[2000], *rfpath, *p_path, temp_p_path[1000] = "", *p_temp_p_path;
    if (!strcmp(path, "/"))
    {
        sprintf(fpath, "%s", dirpath);
    }
    else
    {
        if (strstr(path, "Animeku_"))
        {
            p_path = strrchr(path, '/');
            strncpy(temp_p_path, path, strlen(path) - strlen(p_path));
            p_temp_p_path = strrchr(temp_p_path, '/');
            if (strstr(p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
            }
            else if (strstr(p_temp_p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "", srcFileName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                char *p_ext;
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                p_ext = strrchr(p_srcFileName, '.');
                // if file has extension (.txt, .zip, ...)
                if (p_ext)
                {
                    strncpy(srcFileName, p_srcFileName, strlen(p_srcFileName) - strlen(p_ext));
                    sprintf(fpath, "%s%s%s%s", dirpath, srcDirName, cipher(srcFileName), p_ext);
                }
                else
                {
                    char testDirPath[] = "";
                    sprintf(testDirPath, "%s%s", dirpath, path);
                    DIR *dp = opendir(testDirPath);
                    if (dp)
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
                    else
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, cipher(p_srcFileName));
                }
            }
            else
            {
                sprintf(fpath, "%s%s", dirpath, path);
            }
        }
        else
        {
            sprintf(fpath, "%s%s", dirpath, path);
        }
    }
    rfpath = fpath;
    return rfpath;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000], *p_path;
    bool animeku = strstr(path, "/Animeku_");
    strcpy(fpath, handlePath(path));
    (void)offset;
    (void)fi;
    int res = 0;
    DIR *dp;
    struct dirent *de;
    dp = opendir(fpath);
    if (dp == NULL)
        return errno;
    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        {
            res = (filler(buf, de->d_name, &st, 0));
        }
        else if (animeku)
        {
            if (de->d_type & DT_DIR)
            {
                char temp[1000];
                strcpy(temp, de->d_name);
                res = (filler(buf, temp, &st, 0));
            }
            else
            {
                char *p_ext;
                p_ext = strrchr(de->d_name, '.');
                char fileName[1000] = "";
                if (p_ext)
                {
                    strncpy(fileName, de->d_name, strlen(de->d_name) -

                                                      strlen(p_ext));

                    strcpy(fileName, cipher(fileName));
                    strcat(fileName, p_ext);
                }
                else
                {
                    strcpy(fileName, cipher(de->d_name));
                }
                res = (filler(buf, fileName, &st, 0));
            }
        }
        if (de->d_type & DT_DIR)
        {
            char temp[1000];
            strcpy(temp, de->d_name);
            res = (filler(buf, temp, &st, 0));
        }
        else
            res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    strcpy(fpath, handlePath(path));
    int fd;
    int res;
    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);
    return res;
}
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    char fpath[1000];
    strcpy(fpath, handlePath(path));
    int res;
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_rename(const char *old, const char *new){
    char fPathOld[1000], fPathNew[1000], *p_fPathOld, *p_fPathNew;

    if(!strcmp(old, "/")){
        sprintf(fPathOld, "%s", dirpath);
    }
    else{
        sprintf(fPathOld, "%s%s", dirpath, old);
    }

     if(!strcmp(new, "/")){
        sprintf(fPathNew, "%s", dirpath);
    }
    else{
        sprintf(fPathNew, "%s%s", dirpath, new);
    }

    if(rename(fPathOld, fPathNew)==-1){
        return errno;
    }
    p_fPathOld = strrchr(fPathOld, '/');
    p_fPathNew = strrchr(fPathNew, '/');
    if(strstr(p_fPathOld, "Animeku_")) writeLog("terdecoode", fPathOld, fPathNew);
    if(strstr(p_fPathNew, "Animeku_")) writeLog("terenkripsi", fPathOld, fPathNew);
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}